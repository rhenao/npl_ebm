function exp_npl_class()
%
%
%

%#ok<*UNRCH>

close all
clc

data.xpr = readtable( './data/npl_mrm.csv', 'readrownames', true );
data.sid = data.xpr.Properties.VariableNames;
data.anno = readtable( './data/npl_anno.csv' );
data.key = readtable( './data/npl_key.csv' );
data.xpr = data.xpr{:,:};

% basic summary
summary( data );

% missing values
data = impute( data );

% transform
data.xpr = log( data.xpr );

% batch correction
cohort = nominal( data.key.Challenge );
ucohort = getlevels( cohort );
for j=1:numel( ucohort )
	ix = cohort == ucohort(j);
	data.xpr(:,ix) = bsxfun( @minus, data.xpr(:,ix), mean( data.xpr(:,ix), 2 ) );
end

% get some data from key
status_time = reorderlevels( nominal( data.key.TIME ), { 'BL' 'T' } );
status_inf = reorderlevels( nominal( data.key.SHEDDING ), { 'Non-inf' 'Inf' } );
status_sx = reorderlevels( nominal( data.key.SYMPTOMATIC ), { 'Asx' 'Sx' } );
status_gea = reorderlevels( nominal( data.key.GEA ), { '0' '1' } );

% status_sx_bk = status_sx;
status_sx_bk = status_inf;
ix = double( status_sx ) ~= double( status_inf );
status_sx(ix) = nominal( double( status_gea(ix) ), { 'Asx' 'Sx' } );
status_inf(ix) = nominal( double( status_gea(ix) ), { 'Non-inf' 'Inf' } );
fprintf( 'ties: replaced %d labels of %d\n', sum( status_sx(ix) ~= status_sx_bk(ix) ), sum(ix) )
y = strcat( cellstr( status_time ), cellstr( status_sx ) );
cases = 'TSx'; exclude = 'BLAsx';

% fit one
%
% nested leave-one-out takes a very long time
%
do_nested_loo = false;
if do_nested_loo
	iv = ~ismember( y, exclude );
	[ yp auc auc_one nz tpr tnr ] = fit( data.xpr(:,iv), strcmp( y(iv), cases ), true, 1 );
else
	res = fit_one( data.xpr, strcmp( y, cases ), ~ismember( y, exclude ), data.anno );
	
	% more numbers
	err = ( res.ypp_all >= res.tr_loo(3) ) ~= strcmp( y, cases );
	print_crosstab( data.key.EARLY, err )
	print_crosstab( data.key.SHAM, err )
	
	status_sham = cellstr( status_sx ); status_sham(strcmp( data.key.SHAM, 'Sham' )) = { 'Sham' }; status_sham = nominal( status_sham );
	status_sham = setlabels( status_sham, { 'Uninf' 'Sham' 'Inf' } );
	cohort = setlabels( cohort, { 'H3N2 #1' 'H3N2 #2' 'HRV #1' 'HRV #2' } );
	plot_preds( cohort, status_sham, status_inf, data.key.TIME, data.key.Patient_ID, res.ypp_all, res.tr_loo(3), err );
	
	[ xx yy ~, auc_one ] = perfcurve( strcmp( y(~ismember( y, exclude )), cases ), res.ypp_all(~ismember( y, exclude )), true );
	[ ~, iy ] = min( xx.^2 + ( yy - 1 ).^2 );
	figure, hold on
	plot( xx, yy, 'LineWidth', 2 )
	plot( xx(iy), yy(iy), 'ro', 'LineWidth', 2 )
	plot( [ 0 1 ], [ 0 1 ], 'k--', 'LineWidth', 2 )
	hold off, axis square, grid on, box on
	set( gca, 'FontSize', 16 ), xlabel( '1 - Specificity (FPR)' ), ylabel( 'Sensitivity (TPR)' )
	title( sprintf( 'AUC: %1.3f', auc_one ) )
	
	% print signature
	str = regexprep( data.anno.Peptide_Protein(res.nz), '\|\w+_HUMAN', '' );
	fprintf( 'Signature:\n' )
	fprintf( '%s\n', str{:} )
	
	str = regexprep( data.anno.Peptide_Protein(res.nz), '[A-Z0-9]+\||_HUMAN', '' );
	fprintf( '\npeptides: %d, proteins: %d\n', numel( str ), numel( unique( str ) ) )
end

end

function summary( data )
%
% basic data summary
%

print_crosstab( data.key.Challenge, data.key.TIME )
print_crosstab( data.key.Challenge, data.key.SYMPTOMATIC )
print_crosstab( data.key.Challenge, data.key.SHEDDING )
print_crosstab( data.key.Challenge, data.key.GEA )

print_crosstab( data.key.TIME, data.key.SYMPTOMATIC )
print_crosstab( data.key.TIME, data.key.SHEDDING )
print_crosstab( data.key.TIME, data.key.GEA )

[ tb ~, ~, labels ] = crosstab( data.key.Challenge, data.key.Patient_ID );
for i=1:size( tb, 1 )
	fprintf( '%s\t%d\n', labels{i,1}, sum( tb(i,:) > 0 ) )
end

us = unique( data.key.Challenge );
for i=1:numel( us )
	fprintf( 'Challenge: %s\n', us{i} )
	t_ = data.key(strcmp( data.key.Challenge, us(i) ),:);
	[ ~, iy ] = unique( t_.Patient_ID );
	fprintf( '\tN: %d\n', size( t_(iy,:),1 ) )
	fprintf( '\tSx: %d\n', sum( strcmp( t_.SYMPTOMATIC(iy), 'Sx' ) ) )
	fprintf( '\tInf: %d\n', sum( strcmp( t_.SHEDDING(iy), 'Inf' ) ) )
	fprintf( '\tSx-Non-inf: %d\n', sum( strcmp( t_.SYMPTOMATIC(iy), 'Sx' ) & strcmp( t_.SHEDDING(iy), 'Non-inf' ) ) )
	fprintf( '\tAsx-Inf: %d\n', sum( strcmp( t_.SYMPTOMATIC(iy), 'Asx' ) & strcmp( t_.SHEDDING(iy), 'Inf' ) ) )
end

end

function print_crosstab( a, b )
%
% print table
%

[ tb ~, ~, labels ] = crosstab( a, b );
fprintf( ' \t%s\n', sprintf( '\t%s', labels{1:size( tb, 2 ),2} ) )
for i=1:size( tb, 1 )
	fprintf( '%s%s\n', labels{i,1}, sprintf( '\t%d', tb(i,:) ) )
end
fprintf( '\n' )

end

function res = fit_one( X, y, ix, anno )
%
% leave-one-out cross-validation
%

% loo training
oo = cvglmnet( X(:,ix)', y(ix), 'binomial', struct( 'nlambda', 100, 'alpha', 1 ), 'class', sum( ix ), [], false, false, false );
res.tr_loo = oo.inj.opt;

[ xx yy tt ] = perfcurve( y(ix), oo.inj.predmat(:,oo.inj.opt(2)), true );
[ ~, iy ] = min( xx.^2 + ( yy - 1 ).^2 );
res.tr_loo(3) = mean( tt(iy:iy+1) );
res.cm = confusionmat( y(ix), oo.inj.predmat(:,oo.inj.opt(2)) >= tt(iy) );

% predict
if any( ~ix )
	yp = glmnetPredict( oo.glmnet_fit, X(:,~ix)', [], 'response' );
	yp = yp(:,oo.inj.opt(2));
end

% split predictions in subgroups
ypp = zeros( size( ix ) );
ypp(ix) = oo.inj.predmat(:,oo.inj.opt(2));
if any( ~ix )
	ypp(~ix) = yp;
	
	err = sum( ( yp >= oo.inj.opt(3) ) ~= y(~ix) );
	fprintf( 'err: %d of %d, %1.4f\n', err, sum( ~ix ), err/sum( ~ix ) )
end

res.ypp_all = ypp;
res.nz = oo.glmnet_fit.beta(:,oo.inj.opt(2)) ~= 0;

fprintf( 'Weight & Protein \\\\\n' )
for i = find( res.nz )'
	fprintf( '%1.3f &  %s \\\\\n', oo.glmnet_fit.beta(i,oo.inj.opt(2)), regexprep( anno.Peptide_Protein{i}, '_HUMAN', '' ) );
end
fprintf( '\n' )

res.sig = table();
res.sig.pid = regexprep( anno.Peptide_Protein(res.nz), '_HUMAN', '' );
res.sig.seq = anno.Sequence(res.nz);
res.sig.weight = oo.glmnet_fit.beta(res.nz,oo.inj.opt(2));

end

function [ yp auc auc_one nz tpr tnr ] = fit( X, y, pos_class, alpha, max_d )
%
% nested leave-one-out cross-validation
%

if nargin < 5
	max_d = size( X, 1 );
end
if nargin < 4
	alpha = 0.5;
end

rng( 0 )

nf = numel( y ) - 1;

yb = y == pos_class;

cvo = cvpartition( numel( y ), 'leaveout' );

[ yp lambda auc tpr tnr ] = deal( zeros( numel( y ), 1 ) );
nz = zeros( size( X, 1 ), 1 );
for n=1:numel( y )
	oo = cvglmnet( X(:,cvo.training( n ))', yb(cvo.training( n )), 'binomial', struct( 'nlambda', 20, 'alpha', alpha ), 'class', nf, [], false, true, false );
	
	pp = glmnetPredict( oo.glmnet_fit, X(:,cvo.test( n ))', [], 'response' );
	
	iz = sum( oo.glmnet_fit.beta ~= 0, 1 ) <= max_d;
	[ ~, ix ] = max( oo.inj.auc.*iz' );
	
	lambda(cvo.test( n )) = oo.lambda(ix);
	auc(cvo.test( n )) = oo.inj.auc(ix);
	tpr(cvo.test( n )) = oo.inj.tpr(ix);
	tnr(cvo.test( n )) = oo.inj.tnr(ix);
	yp(cvo.test( n )) = pp(ix);
	nz = nz + ( oo.glmnet_fit.beta(:,ix) ~= 0 );
end

nz = nz/numel( y );

[ xx yy tt auc_one ] = perfcurve( yb, yp, true );
[ ~, iy ] = min( xx.^2 + ( yy - 1 ).^2 );
fprintf( 'auc: %1.3f\n', auc_one )
fprintf( 'thr: %g, tpr: %1.3f tnr: %1.3f index: %d\n', tt(iy), yy(iy), 1 - xx(iy), iy )

end

function data = impute( data )
%
% impute missing values due to low abundance
%

% drop too many nans
ix = sum( isnan( data.xpr ), 2 ) > 2;
data.anno(ix,:) = [];
data.xpr(ix,:) = [];
fprintf( 'dropped %d variables due to NaN\n', sum( ix ) );

% impute zeros and remaining nans
for i=1:size( data.xpr, 1 )
	ix = data.xpr(i,:) == 0;
	iy = isnan( data.xpr(i,:) );
	
	data.xpr(i,ix) = 0.5*min( data.xpr(i,~ix) );
	data.xpr(i,iy) = 0.5*min( data.xpr(i,~iy) );
end

end

