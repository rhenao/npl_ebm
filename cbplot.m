function cbplot( X, y )
%
%
%

if ~isa( y, 'nominal' )
	y = nominal( y );
end

yl = getlevels( y );

col = lines( numel( yl ) );
figure
hold on
for i=1:numel( yl )
	ix = y == yl(i);
	plot( i + 0.4*rand( sum( ix ), 1 ) - 0.2, X(ix), 'o', 'color', [ 0.4 0.4 0.4 ], 'markerfacecolor', col(i,:), 'markersize', 10 )
end
boxplot( X, y, 'color', 'k', 'symbol', 'k+' )
hold off
box on
set( gca, 'fontsize', 18 )

