function plot_preds( batch, state, istate, time, pid, ypp, thr, err )
%
%
%

col = [ 0 134 202; 60 114 0; 218 40 27; 100 100 100 ]/255;

% nominals
batch = nominal( batch ); ubatch = getlevels( batch );
state = nominal( state  ); ustate = getlevels( state );
istate = nominal( istate ); % uistate = getlevels( istate );
time = nominal( time ); % utime = getlevels( time );
pid = nominal( pid ); % upid = getlevels( pid );

ln = { '--' '-' ':' };
mk = { 'x' 'o' };

xl = {};
offset = 0;

figure( 'Position', [ 0 0 800 350 ] )
axes( 'Position', [ 0.07 0.11 0.9 0.82 ] )
hold on
for i=1:numel( ubatch )
	count = 0;
	for j=1:numel( ustate )
		ix = batch == ubatch(i) & state == ustate(j);
		if any( ix )
			lpid = droplevels( pid(ix) ); ulpid = getlevels( lpid );
			ltime = time(ix);
			lypp = ypp(ix);
			lerr = err(ix);
			listate = istate(ix);
			for k=1:numel( ulpid )
				iz = find( lpid == ulpid(k) );
				il = unique( double( listate(iz) ) );
				plot( double( ltime(iz) ) + offset, lypp(iz), ln{il}, 'LineWidth', 2, 'Color', col(j,:) )
				for l=1:numel( iz )
					plot( double( ltime(iz(l)) ) + offset, lypp(iz(l)), 'LineWidth', 2, 'Marker', mk{~lerr(iz(l)) + 1}, 'Color', col(j,:) )
				end
			end
			offset = offset + 2;
			count = count + 1;
			
			xl = [ xl char( ustate(j) ) ]; %#ok
		end
	end
	plot( [ offset offset ] + 0.5, [ 0 1 ], 'k:' )
	text( offset - count + 0.5, 1.05, char( ubatch(i) ), 'HorizontalAlignment', 'center', 'FontSize', 14 )
end
plot( xlim, thr*ones( 1, 2 ), ':', 'LineWidth', 2, 'Color', [ 0.6 0.6 0.6 ] )
hold off
% grid on
box on
xlim( [ 0.8 offset + 0.2 ] )
ylim( [ 0 1 ] )
set( gca, 'FontSize', 14, 'XTick', 1.5:2:offset, 'XTickLabel', xl )
ylabel( 'Probability of Positive, p(Inf-T)' )
xlabel( 'status' )

end
