function exp_npl_uni
%
%
%

close all
clc

data.xpr = readtable( './data/npl_mrm.csv', 'readrownames', true );
data.sid = data.xpr.Properties.VariableNames;
data.anno = readtable( './data/npl_anno.csv' );
data.key = readtable( './data/npl_key.csv' );
data.xpr = data.xpr{:,:};

% summary
summary( data );

% missing values
data = impute( data );

% some scatter plots
ix = strcmp( data.key.Challenge, 'H3N2 DEE2' );

% up
list = [ 9 47 38 39 6 19 14 ];
for i = list
	cbplot( log2( exp(data.xpr(i,ix)) ), data.key.Original_Treatment_Group(ix) )
	ylabel( 'Ratio (light:heavy)' ), title( data.anno{i,1} ), axis square
	ha = gca; ha.XTickLabelRotation = 12; ha.FontSize = 24;
end

% down
list = [ 17 34 35 40 ];
for i = list
	cbplot( log2( exp(data.xpr(i,ix)) ), data.key.Original_Treatment_Group(ix) )
	ylabel( 'Ratio (light:heavy)' ), title( data.anno{i,1} ), axis square
	ha = gca; ha.XTickLabelRotation = 12; ha.FontSize = 24;
end

% transform
data.xpr = log( data.xpr );

% testing
tb_all = uni_test( data.anno, data.key, data.xpr, 'Sx' );
writetable( tb_all, './res_vnltp_mrm_univariate.csv' );

ix = ismember( data.key.Challenge, { 'H3N2 DEE2' 'H3N2 DEE5' } );
tb_flu = uni_test( data.anno, data.key(ix,:), data.xpr(:,ix), 'Sx' );
ix = ismember( data.key.Challenge, { 'HRV 2007' 'HRV 2010' } );
tb_hrv = uni_test( data.anno, data.key(ix,:), data.xpr(:,ix), 'Sx' );

fprintf( 'flu: %d hrv: %d inter: %d union: %d\n', ...
	sum( tb_flu.FDR < 0.05 ), sum( tb_hrv.FDR < 0.05 ), sum( tb_flu.FDR < 0.05 & tb_hrv.FDR < 0.05 ), sum( tb_flu.FDR < 0.05 | tb_hrv.FDR < 0.05 ) )

fprintf( 'correlation: %g\n', corr( tb_flu.es, tb_hrv.es ) )

col = lines( 4 );
figure, hold on
iu = tb_flu.FDR < 0.05 & tb_hrv.FDR < 0.05;
plot( tb_flu.es(iu), tb_hrv.es(iu), 'o', 'linewidth', 2, 'color', col(1,:) )
iu = tb_flu.FDR < 0.05 & tb_hrv.FDR >= 0.05;
plot( tb_flu.es(iu), tb_hrv.es(iu), 'o', 'linewidth', 2, 'color', col(2,:) )
iu = tb_flu.FDR >= 0.05 & tb_hrv.FDR < 0.05;
plot( tb_flu.es(iu), tb_hrv.es(iu), 'o', 'linewidth', 2, 'color', col(3,:) )
iu = tb_flu.FDR >= 0.05 & tb_hrv.FDR >= 0.05;
plot( tb_flu.es(iu), tb_hrv.es(iu), 'o', 'linewidth', 2, 'color', col(4,:) )
mdl = fitlm( tb_flu.es, tb_hrv.es );
plot( xlim()', predict( mdl, xlim()' ), 'k--', 'linewidth', 2 )
hold off
box on, grid on, set( gca, 'fontsize', 14 )
xlabel( 'Effect Size: H3N2' ), ylabel( 'Effect Size: HRV' )
h = legend( { 'H3N2 and HRV' 'H3N2' 'HRV' 'None' }, 'location', 'southeast' );
text( 'Parent', h.DecorationContainer, 'String', 'FDR < 0.05', ...
	'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', ...
	'Position', [ 0.5, 1.05, 0 ], 'Units', 'normalized', 'FontSize', 14 );

end

function tb = uni_test( anno, key, xpr, t )
%
%
%

tp = strcmp( key.SYMPTOMATIC, t );

warning( 'off', 'stats:LinearModel:RankDefDesignMat' )
warning( 'off', 'stats:glmfit:IterationLimit' )
[ pv es ] = deal( zeros( size( xpr, 1 ), 1 ) );
for i=1:size( xpr, 1 )
	tmp = key(tp,{ 'Challenge' 'TIME' }); tmp.xpr = xpr(i,tp)';
	tmp.TIME = strcmp( tmp.TIME, 'T' );
	mdl = fitglm( tmp, 'TIME ~ Challenge + xpr', 'distribution', 'binomial', 'link', 'logit' );
	pv(i) = mdl.Coefficients.pValue(end);
	es(i) = mdl.Coefficients.Estimate(end);
end
warning( 'on', 'stats:LinearModel:RankDefDesignMat' )
warning( 'on', 'stats:glmfit:IterationLimit' )

sp = strcmp( key.TIME, 'T' );

tb = cell2table( [ regexprep( anno.Peptide_Protein, '\|[\w\d]+_HUMAN$', '' ) regexprep( anno.Peptide_Protein, '^[\w\d]+\||_HUMAN', '' )  ] );
tb.Properties.VariableNames = { 'Uniprot' 'Symbol' };
tb.fc = exp( mean( xpr(:,tp & sp), 2 ) - mean( xpr(:,tp & ~sp), 2 ) );
tb.es = es;
tb.pVal = pv;
tb.FDR = mafdr( pv, 'BHFDR', true );

[ ~, ix ] = sort( tb.Symbol );
tb = tb(ix,:);

end

function summary( data )
%
% basic data summary
%

print_crosstab( data.key.Challenge, data.key.TIME )
print_crosstab( data.key.Challenge, data.key.SYMPTOMATIC )
print_crosstab( data.key.Challenge, data.key.SHEDDING )
print_crosstab( data.key.Challenge, data.key.GEA )

print_crosstab( data.key.TIME, data.key.SYMPTOMATIC )
print_crosstab( data.key.TIME, data.key.SHEDDING )
print_crosstab( data.key.TIME, data.key.GEA )

[ tb ~, ~, labels ] = crosstab( data.key.Challenge, data.key.Patient_ID );
for i=1:size( tb, 1 )
	fprintf( '%s\t%d\n', labels{i,1}, sum( tb(i,:) > 0 ) )
end

us = unique( data.key.Challenge );
for i=1:numel( us )
	fprintf( 'Challenge: %s\n', us{i} )
	t_ = data.key(strcmp( data.key.Challenge, us(i) ),:);
	[ ~, iy ] = unique( t_.Patient_ID );
	fprintf( '\tN: %d\n', size( t_(iy,:),1 ) )
	fprintf( '\tSx: %d\n', sum( strcmp( t_.SYMPTOMATIC(iy), 'Sx' ) ) )
	fprintf( '\tInf: %d\n', sum( strcmp( t_.SHEDDING(iy), 'Inf' ) ) )
	fprintf( '\tSx-Non-inf: %d\n', sum( strcmp( t_.SYMPTOMATIC(iy), 'Sx' ) & strcmp( t_.SHEDDING(iy), 'Non-inf' ) ) )
	fprintf( '\tAsx-Inf: %d\n', sum( strcmp( t_.SYMPTOMATIC(iy), 'Asx' ) & strcmp( t_.SHEDDING(iy), 'Inf' ) ) )
end

end

function data = impute( data )
%
% impute missing values due to low abundance
%

% drop too many nans
ix = sum( isnan( data.xpr ), 2 ) > 2;
data.anno(ix,:) = [];
data.xpr(ix,:) = [];
fprintf( 'dropped %d variables due to NaN\n', sum( ix ) );

% impute zeros and remaining nans
for i=1:size( data.xpr, 1 )
	ix = data.xpr(i,:) == 0;
	iy = isnan( data.xpr(i,:) );
	
	data.xpr(i,ix) = 0.5*min( data.xpr(i,~ix) );
	data.xpr(i,iy) = 0.5*min( data.xpr(i,~iy) );
end

end
