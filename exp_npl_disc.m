function exp_npl_disc
%
%
%

close all
clc

% load data
warning( 'off', 'MATLAB:table:ModifiedVarnames' )
rd = readtable( './data/2693_IG_stats_072111.xlsx' );
warning( 'on', 'MATLAB:table:ModifiedVarnames' )

up = unique( rd.PrimaryProteinName );
tt = lower( regexprep( rd.Properties.VariableNames(14:end), 'ID\d+_\d+_|A?[Ss]ymptomatic_', '' ) );
ss = lower( regexprep( rd.Properties.VariableNames(14:end), 'ID\d+_\d+_(Baseline|TimeT)|_', '' ) );

X = rd{:,14:end};
for i=1:size( X, 1 )
	X(i,X(i,:) == 0) = 0.1*min( X(i,X(i,:) > 0) );
end

fprintf( 'Unique peptides: %d peptide seqs: %d proteins: %d\n', ...
		 numel( unique( rd.PeptideName ) ), numel( unique( rd.ModifiedPeptideSequence ) ), numel( unique( rd.PrimaryProteinName ) ) )

mm = nanmedian( X, 2 );
[ ~, ix ] = sort( mm );
ix = ix(floor( 0.1*numel( mm ) ):ceil( 0.9*numel( mm ) ));
nn = nanmedian( X(ix,:), 1 );
X = bsxfun( @times, X, mean( nn )./nn );

% labels = strcat( cellstr( nominal( ss', { 'Asx' 'Sx' } ) ), ', ', cellstr( nominal( tt', { 'Baseline' 'Time T' } ) ) );
labels = strcat( cellstr( nominal( ss', { 'Uninf' 'Inf' } ) ), '-', cellstr( nominal( tt', { 'BL' 'T' } ) ) );
labels = reorderlevels( nominal( labels ), { 'Uninf-BL' 'Uninf-T' 'Inf-BL' 'Inf-T' } );
list = { 'CFAB_HUMAN' 'TIG1_HUMAN' 'TBA1B_HUMAN' 'STAT_HUMAN' 'ANXA2_HUMAN' 'LCN15_HUMAN' };
for i=1:numel( list )
	ix = strcmp( rd.PrimaryProteinName, list{i} );
	xx = sum( X(ix,:), 1 );
	cbplot( xx, nominal( labels ) )
	ylabel( 'Protein Intensity' ), title( sprintf( '%d Peptides', sum( ix ) ) ), axis square
	ha = gca; ha.XTickLabelRotation = 12; ha.FontSize = 24;
end

tb = cell( numel( up ), 8 );
for i=1:numel(  up )
	ix = strcmp( rd.PrimaryProteinName, up(i) );
	
	np = numel( unique( rd.PeptideName(ix) ) );
	% np = numel( unique( pp(ix) ) );
	
	xx = sum( X(ix,:), 1 );
	f1 = mean( xx(strcmp( ss, 'symptomatic' ) & strcmp( tt, 'timet' ))./xx(strcmp( ss, 'symptomatic' ) & strcmp( tt, 'baseline' )) );
	f2 = mean( xx(strcmp( ss, 'asymptomatic' ) & strcmp( tt, 'timet' ))./xx(strcmp( ss, 'asymptomatic' ) & strcmp( tt, 'baseline' )) );
	f3 = mean( xx(strcmp( ss, 'symptomatic' ) & strcmp( tt, 'timet' ))./xx(strcmp( ss, 'asymptomatic' ) & strcmp( tt, 'timet' )) );
	p1 = anova1( log( xx(strcmp( ss, 'symptomatic' )) ), tt(strcmp( ss, 'symptomatic' )), 'off' );
	
	
	tb(i,:) = [ up(i) unique( rd.ProteinDescription(ix) ) sum( ix ) np f1 f2 f3 p1 ];
	% tb(i,:) = [ up(i) unique( pd(ix) ) sum( ix ) np f1 f2 f3 p1 ];
end

tb = cell2table( tb, 'VariableNames', { 'PrimaryProteinName' 'ProteinDescription' 'NPeptides' 'NUniquePeptides' 'fcSX_BvsT' 'fcASX_BvsT' 'fcT_ASXvsSX' 'pSX_BvsT' } );
tb.FC_Ratio = tb.fcSX_BvsT./tb.fcASX_BvsT;
tb.FDRSX_BvsT = mafdr( tb.pSX_BvsT, 'BHFDR', true );

fprintf( 'fold change sx, baseline vs T, up: %d down: %d\n', sum( tb.fcSX_BvsT > 2 ), sum( 1./tb.fcSX_BvsT > 2 ) )
fprintf( 'fold change T, asx vs sx, up: %d down: %d\n', sum( tb.fcT_ASXvsSX > 2 ), sum( 1./tb.fcT_ASXvsSX > 2 ) )

% up
uc1 = tb.fcSX_BvsT > 2 & tb.pSX_BvsT < 0.15;
uc2 = tb.FC_Ratio > 1;
uc3 = tb.fcT_ASXvsSX > 2;
uc4 = tb.NPeptides > 1;
uc_14 = uc1 & uc2 & uc3 & uc4; %#ok

% down
dc1 = 1./tb.fcSX_BvsT >= 2 & tb.pSX_BvsT < 0.15;
dc2 = 1./tb.FC_Ratio >= 1;
dc3 = 1./tb.fcT_ASXvsSX >= 2;
dc4 = tb.NPeptides > 1;
dc_14 = dc1 & dc2 & dc3 & dc4; %#ok

end
